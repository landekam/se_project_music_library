﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.MusicLibraryTests
{
    [TestClass]
    public class MusicLibraryLoadPlaylistTests
    {
        [TestMethod]
        public void ListIsCorrectSize0()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            List<String> testList = new List<String>();
            //Act
            musicLibrary.loadPlaylist(testList);
            int result = musicLibrary.getPathsCount();
            //Assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void ListIsCorrectSize3()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            List<String> testList = new List<String>();
            testList.Add("test1");
            testList.Add("test2");
            testList.Add("test3");
            //Act
            musicLibrary.loadPlaylist(testList);
            int result = musicLibrary.getPathsCount();
            //Assert
            Assert.AreEqual(3,result);
        }

        [TestMethod]
        public void ListLoadsStringsCorrectly()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            List<String> testList = new List<String>();
            testList.Add("test1");
            testList.Add("test2");
            testList.Add("test3");
            //Act
            musicLibrary.loadPlaylist(testList);
            String result1 = musicLibrary.getPathAtIndex(0);
            String result2 = musicLibrary.getPathAtIndex(1);
            String result3 = musicLibrary.getPathAtIndex(2);
            //Assert
            Assert.AreEqual("test1", result1);
            Assert.AreEqual("test2", result2);
            Assert.AreEqual("test3", result3);
        }
    }
}
