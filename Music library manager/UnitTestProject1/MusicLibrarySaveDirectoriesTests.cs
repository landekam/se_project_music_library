﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Music_library_manager;

namespace UnitTestProject1.MusicLibraryTests
{
    [TestClass]
    public class MusicLibrarySaveDirectoriesTests
    {
        
        [TestMethod]
        public void SavesAllDirectoriesInFile()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            StreamWriter streamWriter = new StreamWriter("libraryDirectories.txt");
            streamWriter.WriteLine("test1");
            streamWriter.WriteLine("test2");
            streamWriter.Close();
            musicLibrary.LoadDirectories();
            streamWriter = new StreamWriter("libraryDirectories.txt");
            streamWriter.Close();
            StreamReader streamReader;
            //Act
            musicLibrary.SaveDirectories();
            streamReader = new StreamReader("libraryDirectories.txt");
            String result1 = streamReader.ReadLine();
            String result2 = streamReader.ReadLine();
            streamReader.Close();
            //Assert
            Assert.AreEqual("test1", result1);
            Assert.AreEqual("test2", result2);
        }

        [TestMethod]
        public void SavesNothingIfDirectoriesListIsEmpty()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            StreamWriter streamWriter = new StreamWriter("libraryDirectories.txt");
            streamWriter.Close();
            StreamReader streamReader;
            //Act
            musicLibrary.SaveDirectories();
            streamReader = new StreamReader("libraryDirectories.txt");
            String result = streamReader.ReadLine();
            streamReader.Close();
            //Assert
            Assert.AreEqual(null, result);
        }
    }
    
}
