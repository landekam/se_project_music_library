﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Music_library_manager;

namespace UnitTestProject1.MusicLibraryTests
{
    [TestClass]
    public class MusicLibraryConstructor
    {
        [TestMethod]
        public void PausedIsTrue()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            //Act
            bool result = musicLibrary.isPaused;
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void FirstTimePlayingIsTrue()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            //Act
            bool result = musicLibrary.firstTimePlaying;
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DirectoriesIsEmpty()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            //Act
            int result = musicLibrary.getDirectoriesCount();
            //Assert
            Assert.AreEqual(0,result);
        }

        [TestMethod]
        public void PathsIsEmpty()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            //Act
            int result = musicLibrary.getPathsCount();
            //Assert
            Assert.AreEqual(0, result);
        }
    }
}
