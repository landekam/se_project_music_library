﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Music_library_manager;

namespace UnitTestProject1.MusicLibraryTests
{
    [TestClass]
    public class MusicLibraryLoadDirectoriesTests
    {
        [TestMethod]
        public void LoadsAllLinesInFile()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            StreamWriter streamWriter = new StreamWriter("libraryDirectories.txt");
            streamWriter.WriteLine("test1");
            streamWriter.WriteLine("test2");
            streamWriter.Close();
            //Act
            musicLibrary.LoadDirectories();
            int result = musicLibrary.getDirectoriesCount();
            //Assert
            Assert.AreEqual(result, 2);
        }

        [TestMethod]
        public void LoadedLinesMatchWritten()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            StreamWriter streamWriter = new StreamWriter("libraryDirectories.txt");
            streamWriter.WriteLine("test1");
            streamWriter.WriteLine("test2");
            streamWriter.Close();
            //Act
            musicLibrary.LoadDirectories();
            String result1 = musicLibrary.getDirectoryAtIndex(0);
            String result2 = musicLibrary.getDirectoryAtIndex(1);
            //Assert
            Assert.AreEqual(result1, "test1");
            Assert.AreEqual(result2, "test2");
        }

        [TestMethod]
        public void LoadsNothingFromEmptyFile()
        {
            //Arrange
            Music_library_manager.MusicLibrary musicLibrary = new Music_library_manager.MusicLibrary();
            StreamWriter streamWriter = new StreamWriter("libraryDirectories.txt");
            streamWriter.Close();
            //Act
            musicLibrary.LoadDirectories();
            int result = musicLibrary.getDirectoriesCount();
            //Assert
            Assert.AreEqual(result, 0);
        }
    }
}
