﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Music_library_manager
{
    public partial class FormManageDirectories : Form
    {
        private MusicLibrary musicLibrary = new MusicLibrary();

        public FormManageDirectories()
        {
            InitializeComponent();
            LoadDirectories();
        }

        private void LoadDirectories()
        {

            if (File.Exists("libraryDirectories.txt"))
            {
                musicLibrary.LoadDirectories();

                for (int i = 0; i < musicLibrary.getDirectoriesCount(); i++)
                {
                    listBoxDirectories.Items.Add(musicLibrary.getDirectoryAtIndex(i));
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            musicLibrary.SaveDirectories();
            Close();
            
        }

        private void FormManageDirectories_Load(object sender, EventArgs e)
        {
            Owner.Enabled = false;
        }

        private void FormManageDirectories_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Enabled = true;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    musicLibrary.AddDirectory(folderBrowserDialog.SelectedPath);
                    listBoxDirectories.Items.Add(musicLibrary.getDirectoryAtIndex(musicLibrary.getDirectoriesCount() - 1));
                }
                catch
                {
                    MessageBox.Show("Cannot load from that file path.");
                    return;
                }
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if(listBoxDirectories.SelectedIndex != -1)
            {
                musicLibrary.RemoveDirectory(listBoxDirectories.SelectedItem.ToString());
                listBoxDirectories.Items.RemoveAt(listBoxDirectories.SelectedIndex);
            }
        }
    }
    
}
