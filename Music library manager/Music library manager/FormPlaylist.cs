﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Music_library_manager
{
    public partial class FormPlaylist : Form
    {
        private MusicLibrary musicLibrary = new MusicLibrary();
        bool AddingNewPlaylist = false, RenamingPlaylist = false;
        public FormPlaylist()
        {
            InitializeComponent();
            loadPlaylist();
        }

        private void buttonPlayPause_Click(object sender, EventArgs e)
        {
            PlayPause();
        }
        private void PlayPause()
        {
            if (SongListIsEmpty())
            {
                return;
            }

            if (musicLibrary.firstTimePlaying)
            {
                musicLibrary.firstTimePlaying = false;
                musicLibrary.isPaused = false;
                buttonPlayPause.Image = Properties.Resources.Pause50;

                if (listViewSongs.SelectedItems.Count == 0)
                {
                    windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(0);
                    DisplaySongName(0);
                }
                else
                {
                    windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]));
                    DisplaySongName(listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]));
                }
            }
            else
            {
                if (musicLibrary.isPaused)
                {
                    Play();

                }
                else
                {
                    Pause();
                }
            }
        }

        private void Pause()
        {
            musicLibrary.isPaused = true;
            buttonPlayPause.Image = Properties.Resources.play50;
            windowsMediaPlayer.Ctlcontrols.pause();
        }

        private void Play()
        {
            musicLibrary.isPaused = false;
            buttonPlayPause.Image = Properties.Resources.Pause50;
            windowsMediaPlayer.Ctlcontrols.play();
        }

        private void DisplaySongName(int index)
        {
            TagLib.File file = TagLib.File.Create(musicLibrary.getPathAtIndex(index));
            if (file.Tag.Title == null)
            {
                labelSongCurrent.Text = Path.GetFileName(musicLibrary.getPathAtIndex(index));
            }
            else
            {
                labelSongCurrent.Text = file.Tag.Title;
            }
        }

        public bool SongListIsEmpty()
        {
            if (musicLibrary.getPathsCount() == 0)
            {
                MessageBox.Show("No music in your playlist");
                return true;
            }
            return false;
        }

        private void listViewSongs_DoubleClick(object sender, EventArgs e)
        {
            musicLibrary.firstTimePlaying = false;
            int indexOfSelectedItem = listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]);
            windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(indexOfSelectedItem);
            Play();
            DisplaySongName(indexOfSelectedItem);
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            Pause();
            windowsMediaPlayer.Ctlcontrols.stop();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (SongListIsEmpty())
            {
                return;
            }
            PlayNext();
        }

        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            if (SongListIsEmpty())
            {
                return;
            }
            PlayPrevious();
        }

        private void PlayNext()
        {
            musicLibrary.firstTimePlaying = false;
            String currentSong = windowsMediaPlayer.URL;
            int indexOfCurrentSong = musicLibrary.getIndexOfPath(currentSong);

            if (musicLibrary.getPathsCount() == 0)
            {
                Pause();
                return;
            }

            if (indexOfCurrentSong == musicLibrary.getPathsCount() - 1 || indexOfCurrentSong == -1)
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(0);
                DisplaySongName(0);
            }
            else
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(indexOfCurrentSong + 1);
                DisplaySongName(indexOfCurrentSong + 1);
            }
            Play();

        }

        private void PlayPrevious()
        {
            musicLibrary.firstTimePlaying = false;
            String currentSong = windowsMediaPlayer.URL;
            int indexOfCurrentSong = musicLibrary.getIndexOfPath(currentSong);

            if (indexOfCurrentSong == 0 || indexOfCurrentSong == -1)
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(musicLibrary.getPathsCount() - 1);
                DisplaySongName(musicLibrary.getPathsCount() - 1);
            }
            else
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(indexOfCurrentSong - 1);
                DisplaySongName(indexOfCurrentSong - 1);
            }
            Play();

        }

        private void buttonAddNewPlaylist_Click(object sender, EventArgs e)
        {
            AddingNewPlaylist = true;
            FormNewPlaylist formNewPlaylist = new FormNewPlaylist();
            formNewPlaylist.Owner = this;
            formNewPlaylist.Show();
        }

        private void FormPlaylist_EnabledChanged(object sender, EventArgs e)
        {
            if( Enabled == true && AddingNewPlaylist == true)
            {
                AddingNewPlaylist = false;
                if (!FormNewPlaylist.PlaylistName.Equals(""))
                {
                    SavePlaylist();
                    loadPlaylist();
                }
            }
            if (Enabled == true && RenamingPlaylist == true)
            {
                RenamingPlaylist = false;
                if (!FormNewPlaylist.PlaylistName.Equals(""))
                {
                    RenamePlaylist();
                }
            }
        }

        private void SavePlaylist()
        {
            StreamWriter streamWriter = new StreamWriter("Playlists\\" + FormNewPlaylist.PlaylistName + ".txt");
            streamWriter.Close();
            FormNewPlaylist.PlaylistName = "";
        }

        private void RenamePlaylist()
        {
            int indexOfSelected = listBoxPlaylists.SelectedIndex;
            string[] filePaths = Directory.GetFiles("Playlists");
            File.Move(filePaths[indexOfSelected],"Playlists\\" + FormNewPlaylist.PlaylistName + ".txt");
            FormNewPlaylist.PlaylistName = "";
            loadPlaylist();
        }

        private void loadPlaylist()
        {
            FileInfo file;
            if (!Directory.Exists("Playlists"))
            {
                Directory.CreateDirectory("Playlists");
            }
            string[] filePaths = Directory.GetFiles("Playlists");
            listBoxPlaylists.Items.Clear();
            foreach (string path in filePaths)
            {
                file = new FileInfo(path);
                listBoxPlaylists.Items.Add(file.Name);
            }
        }

        private void buttonRemovePlaylist_Click(object sender, EventArgs e)
        {
            int indexOfSelected = listBoxPlaylists.SelectedIndex;
            if(indexOfSelected == -1)
            {
                return;
            }
            listBoxPlaylists.Items.RemoveAt(indexOfSelected);
            string[] filePaths = Directory.GetFiles("Playlists");
            File.Delete(filePaths[indexOfSelected]);
        }

        private void loadMusic()
        {
            if (listBoxPlaylists.SelectedIndex == -1)
            {
                return;
            }
            if (File.Exists("Playlists\\" + listBoxPlaylists.SelectedItem.ToString()))
            {
                List<string> songs = new List<string>();
                StreamReader streamReader = new StreamReader("Playlists\\" + listBoxPlaylists.SelectedItem.ToString());
                string song;
                while ((song = streamReader.ReadLine()) != null){
                    songs.Add(song);
                }
                streamReader.Close();
                musicLibrary.loadPlaylist(songs);
                listViewSongs.Items.Clear();
                for (int i = 0; i < songs.Count(); i++)
                {
                    TagLib.File file = TagLib.File.Create(songs[i]);

                    String songName = file.Tag.Title;
                    if (songName == null)
                    {
                        songName = Path.GetFileName(songs[i]);
                    }

                    var row = new String[] { songName, file.Properties.Duration.ToString().Substring(3,5)}; //get only minutes and seconds of duration
                    var listViewItem = new ListViewItem(row);
                    listViewItem.Tag = songs[i];

                    listViewSongs.Items.Add(listViewItem);
                }
            }
        }

        private void listBoxPlaylists_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadMusic();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listViewSongs.SelectedItems.Count == 0)
            {
                MessageBox.Show("No song selected");
                return;
            }
            int indexOfSelected = listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]);
            musicLibrary.deleteFromPlaylist(indexOfSelected);
            listViewSongs.Items.RemoveAt(indexOfSelected);
            StreamWriter streamWriter = new StreamWriter("Playlists\\" + listBoxPlaylists.SelectedItem.ToString());
            for (int i = 0; i< listViewSongs.Items.Count; i++)
            {
                streamWriter.WriteLine(musicLibrary.getPathAtIndex(i));
            }
            streamWriter.Close();
        }

        private void trackBarVolume_Scroll(object sender, EventArgs e)
        {
            windowsMediaPlayer.settings.volume = trackBarVolume.Value;
            VolumeDisplay();
        }
        public void VolumeDisplay()
        {
            labelVolume.Text = "Volume " + windowsMediaPlayer.settings.volume + "%";
        }

        private void FormPlaylist_Load(object sender, EventArgs e)
        {
            Owner.Enabled = false;
        }

        private void FormPlaylist_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Enabled = true;
        }

        private void windowsMediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 8)  //media ended
            {
                BeginInvoke(new Action(() => {
                    PlayNext();
                }));
            }
        }

        private void buttonChangeName_Click(object sender, EventArgs e)
        {
            if (listBoxPlaylists.SelectedIndex == -1)
            {
                return;
            }
            RenamingPlaylist = true;
            FormNewPlaylist formNewPlaylist = new FormNewPlaylist();
            formNewPlaylist.Owner = this;
            formNewPlaylist.Show();
        }
    }
}
