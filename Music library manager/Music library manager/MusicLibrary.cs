﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Music_library_manager
{
    public class MusicLibrary
    {
        private List<String> paths;
        private List<String> directories;
        public bool isPaused { get; set; }
        public bool firstTimePlaying{ get; set; }
        public void loadPlaylist(List<String> Playlist) { this.paths = Playlist; }
        public void deleteFromPlaylist(int index) { this.paths.RemoveAt(index); }

        public MusicLibrary()
            {
                paths = new List<String>();
                directories = new List<String>();
                isPaused = true;
                firstTimePlaying = true;
            }

        public String getPathAtIndex(int index)
            {
                return paths[index];
            }

        public void LoadDirectories()
        {
            StreamReader streamReader = new StreamReader("libraryDirectories.txt");
            directories = new List<string>();
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                directories.Add(line);
            }
            streamReader.Close();
        }

        public void LoadPaths()
        {
            paths = new List<String>();
            for (int i = 0; i < directories.Count; i++)
            {
                try
                {
                    paths = paths.Concat(Directory.GetFiles(directories[i], "*.mp3", SearchOption.AllDirectories)
                        .OfType<String>()).ToList();
                }
                catch
                {
                    MessageBox.Show("Can not read from " + directories[i]);
                    continue;
                }
            }
        }

        public int getPathsCount()
        {
            return paths.Count;
        }

        public int getIndexOfPath(String path)
        {
            return paths.IndexOf(path);
        }

        public int getDirectoriesCount()
        {
            return directories.Count;
        }

        public void AddDirectory(String directory)
        {
            directories.Add(directory);
        }

        public void RemoveDirectory(String directory)
        {
            directories.Remove(directory);
        }

        public String getDirectoryAtIndex(int index)
        {
            return directories[index];
        }

        public void SaveDirectories() 
        {
            StreamWriter streamWriter = new StreamWriter("libraryDirectories.txt");
            for (int i = 0; i < getDirectoriesCount(); i++)
            {
                streamWriter.WriteLine(directories[i]);
            }
            streamWriter.Close();
        }
    }
}
