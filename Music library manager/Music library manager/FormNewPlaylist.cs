﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Music_library_manager
{
    public partial class FormNewPlaylist : Form
    {
        public FormNewPlaylist()
        {
            InitializeComponent();
 
        }
        public static string PlaylistName = "";

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            PlaylistName = textBoxName.Text;
            if (PlaylistName.Equals(""))
            {
                MessageBox.Show("Please enter name");
                return;
            }
            Close();
        }

        private void FormNewPlaylist_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            Owner.Enabled = true;
        }

        private void FormNewPlaylist_Load(object sender, EventArgs e)
        {
            Owner.Enabled = false;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
