﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Music_library_manager
{
    public partial class MusicLibraryManager : Form
    {
        
        private MusicLibrary musicLibrary = new MusicLibrary();

        public static string EditMusicNamePath = "";

        public MusicLibraryManager()
        {
            InitializeComponent();
            LoadMusic();
            VolumeDisplay();
        }

        private void buttonManageDirectories_Click(object sender, EventArgs e)
        {
            FormManageDirectories formManageDirectories = new FormManageDirectories();
            formManageDirectories.Owner = this;
            formManageDirectories.Show();
        }

        private void buttonPlayPause_Click(object sender, EventArgs e)
        {
            PlayPause();
        }

        private void listViewSongs_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            musicLibrary.firstTimePlaying = false;
            int indexOfSelectedItem = listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]);
            windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(indexOfSelectedItem);
            Play();
            DisplaySongName(indexOfSelectedItem);
            
        }

        private void windowsMediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 8)  //media ended
            {
                BeginInvoke(new Action(() => {
                    PlayNext();
                }));
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (SongListIsEmpty())
            {
                return;
            }
            PlayNext();
        }

        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            if (SongListIsEmpty())
            {
                return;
            }
            PlayPrevious();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            Pause();
            windowsMediaPlayer.Ctlcontrols.stop();
        }

        private void buttonPlaylists_Click(object sender, EventArgs e)
        {
            Pause();
            FormPlaylist formPlaylist = new FormPlaylist();
            formPlaylist.Owner = this;
            formPlaylist.Show();
        }

        private void PlayPause()
        {
            if (SongListIsEmpty())
            {
                return;
            }
            if (musicLibrary.firstTimePlaying)
            {
                musicLibrary.firstTimePlaying = false;
                musicLibrary.isPaused = false;
                buttonPlayPause.Image = Properties.Resources.Pause50;

                if (listViewSongs.SelectedItems.Count == 0)
                {
                    windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(0);
                    DisplaySongName(0);
                }
                else
                {
                    windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]));
                    DisplaySongName(listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]));
                }
            }
            else
            {
                if (musicLibrary.isPaused)
                {
                    Play();

                }
                else
                {
                    Pause();
                }
            }
        }

        private void Pause()
        {
            musicLibrary.isPaused = true;
            buttonPlayPause.Image = Properties.Resources.play50;
            windowsMediaPlayer.Ctlcontrols.pause();
        }

        private void Play()
        {
            musicLibrary.isPaused = false;
            buttonPlayPause.Image = Properties.Resources.Pause50;
            windowsMediaPlayer.Ctlcontrols.play();
        }

        private void LoadMusic()
        {
            if (File.Exists("libraryDirectories.txt"))
            {
                musicLibrary.LoadDirectories();
                musicLibrary.LoadPaths();

                

                listViewSongs.Items.Clear();
                for (int i = 0; i < musicLibrary.getPathsCount(); i++)
                {
                    TagLib.File file = TagLib.File.Create(musicLibrary.getPathAtIndex(i));

                    String songYear = file.Tag.Year.ToString();
                    if (songYear.Equals("0")) //if year tag is empty put empty string instead of "0"
                    {
                        songYear = "";
                    }
                    String songName = file.Tag.Title;
                    if (songName == null)
                    {
                        songName = Path.GetFileName(musicLibrary.getPathAtIndex(i));
                    }

                    var row = new String[] { songName, file.Tag.FirstPerformer,file.Tag.Album, file.Tag.FirstGenre,songYear,
                        file.Properties.Duration.ToString().Substring(3,5)}; //get only minutes and seconds of duration
                    var listViewItem = new ListViewItem(row);
                    listViewItem.Tag = musicLibrary.getPathAtIndex(i);

                    listViewSongs.Items.Add(listViewItem);
                }


            }            
        }

        private void PlayNext()
        {
            musicLibrary.firstTimePlaying = false;
            String currentSong = windowsMediaPlayer.URL;
            int indexOfCurrentSong = musicLibrary.getIndexOfPath(currentSong);

            if (musicLibrary.getPathsCount() == 0)
            {
                Pause();
                return;
            }

            if (indexOfCurrentSong == musicLibrary.getPathsCount() - 1 || indexOfCurrentSong == -1)
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(0);
                DisplaySongName(0);
            }
            else
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(indexOfCurrentSong + 1);
                DisplaySongName(indexOfCurrentSong + 1);
            }
            Play();

        }

        private void DisplaySongName(int index)
        {
            TagLib.File file = TagLib.File.Create(musicLibrary.getPathAtIndex(index));
            if (file.Tag.Title == null)
            {
                labelSongCurrent.Text = Path.GetFileName(musicLibrary.getPathAtIndex(index));
            }
            else
            {
                labelSongCurrent.Text = file.Tag.Title;
            }
        }

        private void PlayPrevious()
        {
            musicLibrary.firstTimePlaying = false;
            String currentSong = windowsMediaPlayer.URL;
            int indexOfCurrentSong = musicLibrary.getIndexOfPath(currentSong);

            if (indexOfCurrentSong == 0 || indexOfCurrentSong == -1)
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(musicLibrary.getPathsCount() - 1);
                DisplaySongName(musicLibrary.getPathsCount() - 1);
            }
            else
            {
                windowsMediaPlayer.URL = musicLibrary.getPathAtIndex(indexOfCurrentSong - 1);
                DisplaySongName(indexOfCurrentSong - 1);
            }
            Play();

        }

        private void MusicLibraryManager_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled)
            {
                LoadMusic();
            }
        }

        private void trackBarVolume_Scroll(object sender, EventArgs e)
        {
            windowsMediaPlayer.settings.volume = trackBarVolume.Value;
            VolumeDisplay();
        }

        public bool SongListIsEmpty()
        {
            if (musicLibrary.getPathsCount() == 0)
            {
                MessageBox.Show("No music in your library");
                return true;
            }
            return false;
        }

        public void VolumeDisplay()
        {
            labelVolume.Text = "Volume " + windowsMediaPlayer.settings.volume + "%";
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if(listViewSongs.SelectedItems.Count == 0)
            {
                MessageBox.Show("No song selected");
                return;
            }
            EditMusicNamePath = musicLibrary.getPathAtIndex(listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]));
            FormEditID3 formEditID3 = new FormEditID3();
            formEditID3.Owner = this;
            formEditID3.Text = "Edit " + Path.GetFileName(EditMusicNamePath);
            formEditID3.Show();
        }

        private void buttonAddToPlaylist_Click(object sender, EventArgs e)
        {
            if (listViewSongs.SelectedItems.Count == 0)
            {
                MessageBox.Show("No song selected");
                return;
            }
            EditMusicNamePath = musicLibrary.getPathAtIndex(listViewSongs.Items.IndexOf(listViewSongs.SelectedItems[0]));
            FormAddToPlaylist formAddToPlaylist = new FormAddToPlaylist();
            formAddToPlaylist.Owner = this;
            formAddToPlaylist.Show();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            String searchString = textBoxSearch.Text.ToLower();
            if (searchString.Equals(""))
            {
                return;
            }
            else
            {
                List<String> searchResults = new List<string>(); 
                for(int i=0; i < listViewSongs.Items.Count; i++)
                {
                    if (listViewSongs.Items[i].SubItems[0].Text.ToLower().Contains(searchString))
                    {
                        searchResults.Add(musicLibrary.getPathAtIndex(i));
                    }
                }
                musicLibrary.loadPlaylist(searchResults);

                listViewSongs.Items.Clear();
                for (int i = 0; i < searchResults.Count(); i++)
                {
                    TagLib.File file = TagLib.File.Create(searchResults[i]);

                    String songYear = file.Tag.Year.ToString();
                    if (songYear.Equals("0")) //if year tag is empty put empty string instead of "0"
                    {
                        songYear = "";
                    }
                    String songName = file.Tag.Title;
                    if (songName == null)
                    {
                        songName = Path.GetFileName(file.Name);
                    }

                    var row = new String[] { songName, file.Tag.FirstPerformer,file.Tag.Album, file.Tag.FirstGenre,songYear,
                        file.Properties.Duration.ToString().Substring(3,5)}; //get only minutes and seconds of duration
                    var listViewItem = new ListViewItem(row);
                    listViewItem.Tag = searchResults[i];

                    listViewSongs.Items.Add(listViewItem);
                }
            }
        }

        private void buttonClearSearch_Click(object sender, EventArgs e)
        {
            listViewSongs.Items.Clear();
            LoadMusic();
        }
    }
}