﻿namespace Music_library_manager
{
    partial class FormPlaylist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPlaylist));
            this.listBoxPlaylists = new System.Windows.Forms.ListBox();
            this.listViewSongs = new System.Windows.Forms.ListView();
            this.SongName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Lenght = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.windowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.labelSongCurrent = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonRemovePlaylist = new System.Windows.Forms.Button();
            this.buttonAddNewPlaylist = new System.Windows.Forms.Button();
            this.buttonChangeName = new System.Windows.Forms.Button();
            this.labelVolume = new System.Windows.Forms.Label();
            this.trackBarVolume = new System.Windows.Forms.TrackBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPlayPause = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.windowsMediaPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxPlaylists
            // 
            this.listBoxPlaylists.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxPlaylists.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listBoxPlaylists.FormattingEnabled = true;
            this.listBoxPlaylists.ItemHeight = 20;
            this.listBoxPlaylists.Location = new System.Drawing.Point(89, 55);
            this.listBoxPlaylists.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listBoxPlaylists.Name = "listBoxPlaylists";
            this.listBoxPlaylists.Size = new System.Drawing.Size(178, 124);
            this.listBoxPlaylists.TabIndex = 0;
            this.listBoxPlaylists.SelectedIndexChanged += new System.EventHandler(this.listBoxPlaylists_SelectedIndexChanged);
            // 
            // listViewSongs
            // 
            this.listViewSongs.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listViewSongs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.SongName,
            this.Lenght});
            this.listViewSongs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listViewSongs.FullRowSelect = true;
            this.listViewSongs.GridLines = true;
            this.listViewSongs.Location = new System.Drawing.Point(417, 55);
            this.listViewSongs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listViewSongs.Name = "listViewSongs";
            this.listViewSongs.Size = new System.Drawing.Size(631, 379);
            this.listViewSongs.TabIndex = 1;
            this.listViewSongs.UseCompatibleStateImageBehavior = false;
            this.listViewSongs.View = System.Windows.Forms.View.Details;
            this.listViewSongs.DoubleClick += new System.EventHandler(this.listViewSongs_DoubleClick);
            // 
            // SongName
            // 
            this.SongName.Text = "Name";
            this.SongName.Width = 343;
            // 
            // Lenght
            // 
            this.Lenght.Text = "Lenght";
            this.Lenght.Width = 75;
            // 
            // windowsMediaPlayer
            // 
            this.windowsMediaPlayer.Enabled = true;
            this.windowsMediaPlayer.Location = new System.Drawing.Point(13, 580);
            this.windowsMediaPlayer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.windowsMediaPlayer.Name = "windowsMediaPlayer";
            this.windowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("windowsMediaPlayer.OcxState")));
            this.windowsMediaPlayer.Size = new System.Drawing.Size(187, 98);
            this.windowsMediaPlayer.TabIndex = 8;
            this.windowsMediaPlayer.Visible = false;
            this.windowsMediaPlayer.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.windowsMediaPlayer_PlayStateChange);
            // 
            // labelSongCurrent
            // 
            this.labelSongCurrent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelSongCurrent.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelSongCurrent.Location = new System.Drawing.Point(417, 443);
            this.labelSongCurrent.Name = "labelSongCurrent";
            this.labelSongCurrent.Size = new System.Drawing.Size(633, 20);
            this.labelSongCurrent.TabIndex = 12;
            this.labelSongCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRemove.BackColor = System.Drawing.Color.White;
            this.buttonRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRemove.ForeColor = System.Drawing.Color.Tomato;
            this.buttonRemove.Location = new System.Drawing.Point(422, 483);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(98, 77);
            this.buttonRemove.TabIndex = 16;
            this.buttonRemove.Text = "Remove Song";
            this.buttonRemove.UseVisualStyleBackColor = false;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonRemovePlaylist
            // 
            this.buttonRemovePlaylist.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRemovePlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRemovePlaylist.Location = new System.Drawing.Point(88, 245);
            this.buttonRemovePlaylist.Name = "buttonRemovePlaylist";
            this.buttonRemovePlaylist.Size = new System.Drawing.Size(180, 42);
            this.buttonRemovePlaylist.TabIndex = 17;
            this.buttonRemovePlaylist.Text = "Remove playlist";
            this.buttonRemovePlaylist.UseVisualStyleBackColor = true;
            this.buttonRemovePlaylist.Click += new System.EventHandler(this.buttonRemovePlaylist_Click);
            // 
            // buttonAddNewPlaylist
            // 
            this.buttonAddNewPlaylist.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonAddNewPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddNewPlaylist.Location = new System.Drawing.Point(88, 295);
            this.buttonAddNewPlaylist.Name = "buttonAddNewPlaylist";
            this.buttonAddNewPlaylist.Size = new System.Drawing.Size(180, 42);
            this.buttonAddNewPlaylist.TabIndex = 18;
            this.buttonAddNewPlaylist.Text = "Add new playlist";
            this.buttonAddNewPlaylist.UseVisualStyleBackColor = true;
            this.buttonAddNewPlaylist.Click += new System.EventHandler(this.buttonAddNewPlaylist_Click);
            // 
            // buttonChangeName
            // 
            this.buttonChangeName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonChangeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChangeName.Location = new System.Drawing.Point(88, 194);
            this.buttonChangeName.Name = "buttonChangeName";
            this.buttonChangeName.Size = new System.Drawing.Size(180, 42);
            this.buttonChangeName.TabIndex = 19;
            this.buttonChangeName.Text = "Change name";
            this.buttonChangeName.UseVisualStyleBackColor = true;
            this.buttonChangeName.Click += new System.EventHandler(this.buttonChangeName_Click);
            // 
            // labelVolume
            // 
            this.labelVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelVolume.AutoSize = true;
            this.labelVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVolume.Location = new System.Drawing.Point(679, 577);
            this.labelVolume.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(103, 20);
            this.labelVolume.TabIndex = 20;
            this.labelVolume.Text = "Volume 50%";
            // 
            // trackBarVolume
            // 
            this.trackBarVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.trackBarVolume.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.trackBarVolume.LargeChange = 20;
            this.trackBarVolume.Location = new System.Drawing.Point(522, 609);
            this.trackBarVolume.Maximum = 100;
            this.trackBarVolume.Name = "trackBarVolume";
            this.trackBarVolume.Size = new System.Drawing.Size(414, 69);
            this.trackBarVolume.SmallChange = 5;
            this.trackBarVolume.TabIndex = 21;
            this.trackBarVolume.Value = 50;
            this.trackBarVolume.Scroll += new System.EventHandler(this.trackBarVolume_Scroll);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::Music_library_manager.Properties.Resources.MLMlogo200;
            this.pictureBox1.Location = new System.Drawing.Point(78, 394);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 166);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPrevious.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonPrevious.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrevious.FlatAppearance.BorderSize = 0;
            this.buttonPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPrevious.Image = global::Music_library_manager.Properties.Resources.Previous50;
            this.buttonPrevious.Location = new System.Drawing.Point(526, 466);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(124, 97);
            this.buttonPrevious.TabIndex = 15;
            this.buttonPrevious.UseVisualStyleBackColor = false;
            this.buttonPrevious.Click += new System.EventHandler(this.buttonPrevious_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonStop.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonStop.FlatAppearance.BorderSize = 0;
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStop.Image = global::Music_library_manager.Properties.Resources.Next50;
            this.buttonStop.Location = new System.Drawing.Point(922, 469);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(124, 97);
            this.buttonStop.TabIndex = 14;
            this.buttonStop.UseVisualStyleBackColor = false;
            this.buttonStop.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonNext.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNext.Image = global::Music_library_manager.Properties.Resources.Stop50;
            this.buttonNext.Location = new System.Drawing.Point(790, 466);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(124, 97);
            this.buttonNext.TabIndex = 13;
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonPlayPause
            // 
            this.buttonPlayPause.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPlayPause.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPlayPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPlayPause.FlatAppearance.BorderSize = 0;
            this.buttonPlayPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlayPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPlayPause.Image = global::Music_library_manager.Properties.Resources.play50;
            this.buttonPlayPause.Location = new System.Drawing.Point(658, 466);
            this.buttonPlayPause.Name = "buttonPlayPause";
            this.buttonPlayPause.Size = new System.Drawing.Size(124, 97);
            this.buttonPlayPause.TabIndex = 7;
            this.buttonPlayPause.UseVisualStyleBackColor = false;
            this.buttonPlayPause.Click += new System.EventHandler(this.buttonPlayPause_Click);
            // 
            // FormPlaylist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1128, 692);
            this.Controls.Add(this.labelSongCurrent);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.trackBarVolume);
            this.Controls.Add(this.labelVolume);
            this.Controls.Add(this.buttonChangeName);
            this.Controls.Add(this.buttonAddNewPlaylist);
            this.Controls.Add(this.buttonRemovePlaylist);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonPrevious);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.windowsMediaPlayer);
            this.Controls.Add(this.buttonPlayPause);
            this.Controls.Add(this.listViewSongs);
            this.Controls.Add(this.listBoxPlaylists);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(1150, 748);
            this.Name = "FormPlaylist";
            this.Text = "FormPlaylist";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPlaylist_FormClosed);
            this.Load += new System.EventHandler(this.FormPlaylist_Load);
            this.EnabledChanged += new System.EventHandler(this.FormPlaylist_EnabledChanged);
            ((System.ComponentModel.ISupportInitialize)(this.windowsMediaPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxPlaylists;
        private System.Windows.Forms.ListView listViewSongs;
        private System.Windows.Forms.Button buttonPlayPause;
        private AxWMPLib.AxWindowsMediaPlayer windowsMediaPlayer;
        private System.Windows.Forms.Label labelSongCurrent;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.ColumnHeader SongName;
        private System.Windows.Forms.ColumnHeader Lenght;
        private System.Windows.Forms.Button buttonRemovePlaylist;
        private System.Windows.Forms.Button buttonAddNewPlaylist;
        private System.Windows.Forms.Button buttonChangeName;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.TrackBar trackBarVolume;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}