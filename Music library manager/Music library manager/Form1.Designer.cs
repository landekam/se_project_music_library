﻿namespace Music_library_manager
{
    partial class MusicLibraryManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicLibraryManager));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.buttonClearSearch = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.buttonManageDirectories = new System.Windows.Forms.Button();
            this.buttonPlaylists = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelVolume = new System.Windows.Forms.Label();
            this.labelSongCurrent = new System.Windows.Forms.Label();
            this.buttonAddToPlaylist = new System.Windows.Forms.Button();
            this.trackBarVolume = new System.Windows.Forms.TrackBar();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.windowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.buttonPlayPause = new System.Windows.Forms.Button();
            this.listBoxSongs = new System.Windows.Forms.ListBox();
            this.listViewSongs = new System.Windows.Forms.ListView();
            this.SongName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Artist = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Album = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Genre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Year = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Length = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsMediaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelMenu.Controls.Add(this.buttonClearSearch);
            this.panelMenu.Controls.Add(this.pictureBox1);
            this.panelMenu.Controls.Add(this.buttonSearch);
            this.panelMenu.Controls.Add(this.textBoxSearch);
            this.panelMenu.Controls.Add(this.buttonManageDirectories);
            this.panelMenu.Controls.Add(this.buttonPlaylists);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(1304, 62);
            this.panelMenu.TabIndex = 0;
            // 
            // buttonClearSearch
            // 
            this.buttonClearSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonClearSearch.BackColor = System.Drawing.Color.White;
            this.buttonClearSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonClearSearch.Location = new System.Drawing.Point(1054, 3);
            this.buttonClearSearch.Name = "buttonClearSearch";
            this.buttonClearSearch.Size = new System.Drawing.Size(124, 55);
            this.buttonClearSearch.TabIndex = 6;
            this.buttonClearSearch.Text = "Clear Search";
            this.buttonClearSearch.UseVisualStyleBackColor = false;
            this.buttonClearSearch.Click += new System.EventHandler(this.buttonClearSearch_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Music_library_manager.Properties.Resources.MLMlogo50;
            this.pictureBox1.Location = new System.Drawing.Point(15, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 65);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonSearch.BackColor = System.Drawing.Color.White;
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSearch.Location = new System.Drawing.Point(924, 3);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(124, 55);
            this.buttonSearch.TabIndex = 4;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSearch.Location = new System.Drawing.Point(648, 17);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(270, 26);
            this.textBoxSearch.TabIndex = 3;
            // 
            // buttonManageDirectories
            // 
            this.buttonManageDirectories.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonManageDirectories.BackColor = System.Drawing.Color.White;
            this.buttonManageDirectories.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonManageDirectories.Location = new System.Drawing.Point(255, 3);
            this.buttonManageDirectories.Name = "buttonManageDirectories";
            this.buttonManageDirectories.Size = new System.Drawing.Size(124, 55);
            this.buttonManageDirectories.TabIndex = 1;
            this.buttonManageDirectories.Text = "Add music";
            this.buttonManageDirectories.UseVisualStyleBackColor = false;
            this.buttonManageDirectories.Click += new System.EventHandler(this.buttonManageDirectories_Click);
            // 
            // buttonPlaylists
            // 
            this.buttonPlaylists.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPlaylists.BackColor = System.Drawing.Color.White;
            this.buttonPlaylists.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPlaylists.Location = new System.Drawing.Point(386, 3);
            this.buttonPlaylists.Name = "buttonPlaylists";
            this.buttonPlaylists.Size = new System.Drawing.Size(124, 55);
            this.buttonPlaylists.TabIndex = 2;
            this.buttonPlaylists.Text = "Playlists";
            this.buttonPlaylists.UseVisualStyleBackColor = false;
            this.buttonPlaylists.Click += new System.EventHandler(this.buttonPlaylists_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.labelVolume);
            this.panel1.Controls.Add(this.labelSongCurrent);
            this.panel1.Controls.Add(this.buttonAddToPlaylist);
            this.panel1.Controls.Add(this.trackBarVolume);
            this.panel1.Controls.Add(this.buttonStop);
            this.panel1.Controls.Add(this.buttonEdit);
            this.panel1.Controls.Add(this.windowsMediaPlayer);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.buttonPrevious);
            this.panel1.Controls.Add(this.buttonPlayPause);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 319);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1304, 149);
            this.panel1.TabIndex = 1;
            // 
            // labelVolume
            // 
            this.labelVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelVolume.AutoSize = true;
            this.labelVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVolume.Location = new System.Drawing.Point(135, 22);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(65, 20);
            this.labelVolume.TabIndex = 13;
            this.labelVolume.Text = "Volume";
            // 
            // labelSongCurrent
            // 
            this.labelSongCurrent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelSongCurrent.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelSongCurrent.Location = new System.Drawing.Point(15, 15);
            this.labelSongCurrent.Name = "labelSongCurrent";
            this.labelSongCurrent.Size = new System.Drawing.Size(1274, 20);
            this.labelSongCurrent.TabIndex = 11;
            this.labelSongCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonAddToPlaylist
            // 
            this.buttonAddToPlaylist.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonAddToPlaylist.BackColor = System.Drawing.Color.White;
            this.buttonAddToPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddToPlaylist.ForeColor = System.Drawing.Color.Tomato;
            this.buttonAddToPlaylist.Location = new System.Drawing.Point(980, 48);
            this.buttonAddToPlaylist.Name = "buttonAddToPlaylist";
            this.buttonAddToPlaylist.Size = new System.Drawing.Size(138, 55);
            this.buttonAddToPlaylist.TabIndex = 14;
            this.buttonAddToPlaylist.Text = "Add to Playlist";
            this.buttonAddToPlaylist.UseVisualStyleBackColor = false;
            this.buttonAddToPlaylist.Click += new System.EventHandler(this.buttonAddToPlaylist_Click);
            // 
            // trackBarVolume
            // 
            this.trackBarVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.trackBarVolume.BackColor = System.Drawing.SystemColors.ControlDark;
            this.trackBarVolume.LargeChange = 20;
            this.trackBarVolume.Location = new System.Drawing.Point(20, 48);
            this.trackBarVolume.Maximum = 100;
            this.trackBarVolume.Name = "trackBarVolume";
            this.trackBarVolume.Size = new System.Drawing.Size(304, 69);
            this.trackBarVolume.SmallChange = 5;
            this.trackBarVolume.TabIndex = 12;
            this.trackBarVolume.Value = 50;
            this.trackBarVolume.Scroll += new System.EventHandler(this.trackBarVolume_Scroll);
            // 
            // buttonStop
            // 
            this.buttonStop.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonStop.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonStop.FlatAppearance.BorderSize = 0;
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStop.Image = global::Music_library_manager.Properties.Resources.Stop50;
            this.buttonStop.Location = new System.Drawing.Point(849, 34);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(124, 98);
            this.buttonStop.TabIndex = 10;
            this.buttonStop.UseVisualStyleBackColor = false;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEdit.BackColor = System.Drawing.Color.White;
            this.buttonEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEdit.ForeColor = System.Drawing.Color.Tomato;
            this.buttonEdit.Location = new System.Drawing.Point(328, 48);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(124, 55);
            this.buttonEdit.TabIndex = 9;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // windowsMediaPlayer
            // 
            this.windowsMediaPlayer.Enabled = true;
            this.windowsMediaPlayer.Location = new System.Drawing.Point(976, 47);
            this.windowsMediaPlayer.Name = "windowsMediaPlayer";
            this.windowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("windowsMediaPlayer.OcxState")));
            this.windowsMediaPlayer.Size = new System.Drawing.Size(190, 55);
            this.windowsMediaPlayer.TabIndex = 8;
            this.windowsMediaPlayer.Visible = false;
            this.windowsMediaPlayer.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.windowsMediaPlayer_PlayStateChange);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonNext.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Image = global::Music_library_manager.Properties.Resources.Next50;
            this.buttonNext.Location = new System.Drawing.Point(718, 34);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(124, 98);
            this.buttonNext.TabIndex = 7;
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPrevious.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonPrevious.FlatAppearance.BorderSize = 0;
            this.buttonPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrevious.Image = global::Music_library_manager.Properties.Resources.Previous50;
            this.buttonPrevious.Location = new System.Drawing.Point(459, 34);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(124, 98);
            this.buttonPrevious.TabIndex = 5;
            this.buttonPrevious.UseVisualStyleBackColor = false;
            this.buttonPrevious.Click += new System.EventHandler(this.buttonPrevious_Click);
            // 
            // buttonPlayPause
            // 
            this.buttonPlayPause.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPlayPause.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonPlayPause.FlatAppearance.BorderSize = 0;
            this.buttonPlayPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlayPause.Image = global::Music_library_manager.Properties.Resources.play50;
            this.buttonPlayPause.Location = new System.Drawing.Point(590, 34);
            this.buttonPlayPause.Name = "buttonPlayPause";
            this.buttonPlayPause.Size = new System.Drawing.Size(124, 98);
            this.buttonPlayPause.TabIndex = 6;
            this.buttonPlayPause.UseVisualStyleBackColor = false;
            this.buttonPlayPause.Click += new System.EventHandler(this.buttonPlayPause_Click);
            // 
            // listBoxSongs
            // 
            this.listBoxSongs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxSongs.FormattingEnabled = true;
            this.listBoxSongs.ItemHeight = 20;
            this.listBoxSongs.Location = new System.Drawing.Point(0, 62);
            this.listBoxSongs.Name = "listBoxSongs";
            this.listBoxSongs.Size = new System.Drawing.Size(1304, 257);
            this.listBoxSongs.TabIndex = 2;
            // 
            // listViewSongs
            // 
            this.listViewSongs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.SongName,
            this.Artist,
            this.Album,
            this.Genre,
            this.Year,
            this.Length});
            this.listViewSongs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSongs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listViewSongs.FullRowSelect = true;
            this.listViewSongs.GridLines = true;
            this.listViewSongs.Location = new System.Drawing.Point(0, 62);
            this.listViewSongs.MultiSelect = false;
            this.listViewSongs.Name = "listViewSongs";
            this.listViewSongs.Size = new System.Drawing.Size(1304, 257);
            this.listViewSongs.TabIndex = 3;
            this.listViewSongs.UseCompatibleStateImageBehavior = false;
            this.listViewSongs.View = System.Windows.Forms.View.Details;
            this.listViewSongs.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewSongs_MouseDoubleClick);
            // 
            // SongName
            // 
            this.SongName.Text = "Name";
            this.SongName.Width = 200;
            // 
            // Artist
            // 
            this.Artist.Text = "Artist";
            this.Artist.Width = 150;
            // 
            // Album
            // 
            this.Album.Text = "Album";
            this.Album.Width = 150;
            // 
            // Genre
            // 
            this.Genre.Text = "Genre";
            this.Genre.Width = 150;
            // 
            // Year
            // 
            this.Year.Text = "Year";
            this.Year.Width = 100;
            // 
            // Length
            // 
            this.Length.Text = "Length";
            this.Length.Width = 100;
            // 
            // MusicLibraryManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1304, 468);
            this.Controls.Add(this.listViewSongs);
            this.Controls.Add(this.listBoxSongs);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1314, 488);
            this.Name = "MusicLibraryManager";
            this.Text = "Music library manager";
            this.EnabledChanged += new System.EventHandler(this.MusicLibraryManager_EnabledChanged);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsMediaPlayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonPlaylists;
        private System.Windows.Forms.Button buttonManageDirectories;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonPlayPause;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonNext;
        private AxWMPLib.AxWindowsMediaPlayer windowsMediaPlayer;
        private System.Windows.Forms.ListBox listBoxSongs;
        private System.Windows.Forms.ColumnHeader SongName;
        private System.Windows.Forms.ColumnHeader Artist;
        private System.Windows.Forms.ColumnHeader Album;
        private System.Windows.Forms.ListView listViewSongs;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.ColumnHeader Genre;
        private System.Windows.Forms.ColumnHeader Year;
        private System.Windows.Forms.ColumnHeader Length;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelSongCurrent;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.TrackBar trackBarVolume;
        private System.Windows.Forms.Button buttonAddToPlaylist;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonClearSearch;
    }
}

