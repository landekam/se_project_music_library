﻿using System;
using System.Windows.Forms;

namespace Music_library_manager
{
    public partial class FormEditID3 : Form
    {
        public FormEditID3()
        {
            InitializeComponent();
            TagLib.File file = TagLib.File.Create(MusicLibraryManager.EditMusicNamePath);
            textBoxName.Text = file.Tag.Title;
            textBoxArtist.Text = file.Tag.FirstArtist;
            textBoxAlbum.Text = file.Tag.Album;
            textBoxGenre.Text = file.Tag.FirstGenre;
            textBoxYear.Text = file.Tag.Year.ToString();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            TagLib.File file = TagLib.File.Create(MusicLibraryManager.EditMusicNamePath);
            file.Tag.Title = textBoxName.Text;
            file.Tag.Artists = null;
            string[] ArtistName = { textBoxArtist.Text };
            file.Tag.Artists = ArtistName;
            file.Tag.Album = textBoxAlbum.Text;
            string[] Genre = {textBoxGenre.Text};
            file.Tag.Genres = Genre;
            uint x = uint.Parse(textBoxYear.Text);
            file.Tag.Year = x;
            file.Save();
            MessageBox.Show("Successfully Saved!");
            Close();
        }

        private void FormEditID3_Load_1(object sender, EventArgs e)
        {
            Owner.Enabled = false;
        }

        private void FormEditID3_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Enabled = true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
