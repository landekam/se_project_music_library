﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Music_library_manager
{
    public partial class FormAddToPlaylist : Form
    {
        public FormAddToPlaylist()
        {
            InitializeComponent();
            loadPlaylist();
            selectedPath = MusicLibraryManager.EditMusicNamePath;
            
        }
        private string selectedPath = "";
       

        private void loadPlaylist()
        {
            FileInfo file;
            if (!Directory.Exists("Playlists"))
            {
                Directory.CreateDirectory("Playlists");
            }
            string[] filePaths = Directory.GetFiles("Playlists");
            listBoxPlaylists.Items.Clear();
            foreach (string path in filePaths)
            {
                file = new FileInfo(path);
                listBoxPlaylists.Items.Add(file.Name);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormAddToPlaylist_Load(object sender, EventArgs e)
        {
            Owner.Enabled = false;
        }

        private void FormAddToPlaylist_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Enabled = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            FileInfo file;
            string[] filePaths = Directory.GetFiles("Playlists");
            foreach (string path in filePaths)
            {
                file = new FileInfo(path);
                if(listBoxPlaylists.SelectedItem.ToString() == file.Name.ToString())
                {
                    StreamWriter streamWriter = File.AppendText(path);
                    streamWriter.WriteLine(selectedPath);
                    streamWriter.Close();
                    break;
                }
            }
            Close();
        }
    }
}
